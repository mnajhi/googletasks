 package com.googletasks.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.googletasks.MainActivity;
import com.googletasks.google.GoogleHelper;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.google.webrestapi.GsonFactory;
import com.googletasks.ui.viewholders.FragmentAddTasks;
import com.googletasks.ui.views.TitleBar;
import com.najhi.googletaskstest.R;


public class TaskAddFragment extends BaseFragment implements OnClickListener{
	private static final String KEY_TASK_IS_EDIT = "task_is_edit";
	private static final String KEY_TASK = "task";
	private MainActivity mainActObj;
	private TitleBar titleBar;
	private TaskItem task;
	private FragmentAddTasks fragmentAddTask;
	private boolean isEdit;
	private double latitude;
	private double longitude;
	
	public static TaskAddFragment newInstance(boolean isEdit, TaskItem task) {
		TaskAddFragment fragment = new TaskAddFragment( );
		Bundle bundle = new Bundle();
		if(task!=null){
			bundle.putString(KEY_TASK, GsonFactory.getConfiguredGson().toJson(task));
		}
		bundle.putBoolean(KEY_TASK_IS_EDIT, isEdit);
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		mainActObj = getMainActivity();
		if(getArguments()!=null){
			isEdit = getArguments().getBoolean(KEY_TASK_IS_EDIT);
			if(getArguments().containsKey(KEY_TASK)){
				task = GsonFactory.getConfiguredGson().fromJson(getArguments().getString(KEY_TASK), TaskItem.class);
			}
		}
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		View view = inflater.inflate( R.layout.fragment_add_task, null );
		return view;
	}
	
	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		fragmentAddTask = new FragmentAddTasks( view );
		if(isEdit && task!=null){
			fragmentAddTask.edtTitle.setText(task.getTitle());
			fragmentAddTask.edtNotes.setText(task.getNotes());
			fragmentAddTask.btnCreate.setText("Update");
		} else {
			longitude = mainActObj.getLocationHelper().getLongitude();
			latitude = mainActObj.getLocationHelper().getLatitude();
			
			//This below code is just a one more time try to get location
			if(longitude == 0 && latitude == 0){
				Location location = mainActObj.getLocationHelper().getLocation();
				longitude = location.getLongitude();
				latitude = location.getLatitude();
			}
			
			fragmentAddTask.edtNotes.setText("Location Coordinates : " + latitude +"," + longitude );
			
			
		}
		fragmentAddTask.btnCreate.setOnClickListener(this);
	}
	
	@Override
	public void setTitleBar( TitleBar titleBar ) {
		titleBar.hideButtons();
		titleBar.setVisibility( View.VISIBLE );
		if(!isEdit){
			titleBar.setTitleTxt("Create Task");
		} else {
			titleBar.setTitleTxt("Edit Task");
		}
		this.titleBar = titleBar;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		titleBar.setVisibility( View.VISIBLE );
	}

	@Override
	public void setCurrentFrag() {
		
	}

	@Override
	public void setFragmentId() {
		//mainActObj.setLastResumedFragmentID( ID );
		
	}
	
	private boolean validateForm(){
		if(fragmentAddTask.edtTitle.getText().toString().trim().length()==0){
			fragmentAddTask.edtTitle.setError("Required field");
			return false;
		}
		
		if(fragmentAddTask.edtNotes.getText().toString().trim().length()==0){
			fragmentAddTask.edtNotes.setError("Required field");
			return false;
		}
		return true;
	}

	@Override
	public void onClick( View v ) {
		switch ( v.getId() ) {
			case R.id.btnCreate:
				if(isEdit && task!=null){
					if(validateForm()){
						task.setTitle(fragmentAddTask.edtTitle.getText().toString());
						task.setNotes(fragmentAddTask.edtNotes.getText().toString());
						GoogleHelper.getInstance().updateTask(task);
						onBackPressed();
					}
				} else {
					if(validateForm()){
						if(task!=null){
							task.setTitle(fragmentAddTask.edtTitle.getText().toString());
							task.setNotes(fragmentAddTask.edtNotes.getText().toString());
							GoogleHelper.getInstance().insertTask(task);
						}
						onBackPressed();
					}
				}
				break;
		}
	}
	
	@Override
	public void onBackPressed(){
		mainActObj.getSupportFragmentManager().popBackStack();
	}

	@Override
	public void onNetworkConnected() {
		GoogleHelper.getInstance().syncTasksRecusively();
		
	}

	@Override
	public void onNetworkDisconnected() {
		
	}
}
