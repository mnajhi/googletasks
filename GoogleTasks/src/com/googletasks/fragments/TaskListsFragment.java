 package com.googletasks.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.googletasks.MainActivity;
import com.googletasks.google.GoogleHelper;
import com.googletasks.google.GoogleTaskListsListener;
import com.googletasks.google.entities.TaskListItem;
import com.googletasks.ui.adapters.ArrayListAdapter;
import com.googletasks.ui.viewbinders.TaskListsBinder;
import com.googletasks.ui.viewholders.FragmentTasks;
import com.googletasks.ui.views.TitleBar;
import com.najhi.googletaskstest.R;


public class TaskListsFragment extends BaseFragment implements OnClickListener{
	
	private MainActivity mainActObj;
	private TitleBar titleBar;
	private FragmentTasks fragmentTasks;
	private ArrayListAdapter<TaskListItem> adapter;
	
	public static TaskListsFragment newInstance() {
		TaskListsFragment fragment = new TaskListsFragment( );
		return fragment;
	}
	
	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		mainActObj = getMainActivity();
		adapter = new ArrayListAdapter<TaskListItem>(mainActObj, new TaskListsBinder(mainActObj, this));
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		View view = inflater.inflate( R.layout.fragment_tasks, null );
		return view;
	}
	
	private void populateAdapter() {
		GoogleHelper.getInstance().getTasksList(new GoogleTaskListsListener() {
			
			@Override
			public void onSuccess(ArrayList<TaskListItem> taskLists) {
				adapter.clearList();
				adapter.addAll(taskLists);
				if(adapter.getCount()>0){
					fragmentTasks.lstTasks.setVisibility(View.VISIBLE);
					fragmentTasks.txtNoItem.setVisibility(View.GONE);
				} else {
					fragmentTasks.lstTasks.setVisibility(View.GONE);
					fragmentTasks.txtNoItem.setVisibility(View.VISIBLE);
				}
			}
			
			@Override
			public void onFailure(String errorMsg) {
				adapter.clearList();
				fragmentTasks.lstTasks.setVisibility(View.GONE);
				fragmentTasks.txtNoItem.setVisibility(View.VISIBLE);
			}
		});
	}
	
	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		fragmentTasks = new FragmentTasks( view );
		fragmentTasks.lstTasks.setAdapter(adapter);
		fragmentTasks.lstTasks.setVisibility(View.VISIBLE);
		fragmentTasks.txtNoItem.setVisibility(View.GONE);
		populateAdapter();
	}
	
	@Override
	public void setTitleBar( TitleBar titleBar ) {
		titleBar.hideButtons();
		titleBar.setVisibility( View.VISIBLE );
		titleBar.setTitleTxt("Task Lists");
		this.titleBar = titleBar;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		titleBar.setVisibility( View.VISIBLE );
	}

	@Override
	public void setCurrentFrag() {
		
	}

	@Override
	public void setFragmentId() {
		//mainActObj.setLastResumedFragmentID( ID );
		
	}

	@Override
	public void onClick( View v ) {
		
		switch ( v.getId() ) {
			case R.id.relMain:
				mainActObj.addFragment(TasksFragment.newInstance((String)v.getTag(R.string.task_list_id)), "TasksFragment");
				break;
		}
	}
	
	@Override
	public void onBackPressed(){
	}

	@Override
	public void onNetworkConnected() {
		GoogleHelper.getInstance().syncTasksRecusively();
		
	}

	@Override
	public void onNetworkDisconnected() {
		
	}


}
