 package com.googletasks.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.googletasks.MainActivity;
import com.googletasks.google.GoogleHelper;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.google.webrestapi.GsonFactory;
import com.googletasks.ui.viewholders.FragmentTaskDetail;
import com.googletasks.ui.views.TitleBar;
import com.najhi.googletaskstest.R;

public class TaskDetailFragment extends BaseFragment{
	private static final String KEY_TASK = "task";
	private MainActivity mainActObj;
	private TitleBar titleBar;
	private TaskItem task;
	private FragmentTaskDetail fragmentTaskDetail;
	
	public static TaskDetailFragment newInstance(TaskItem task) {
		TaskDetailFragment fragment = new TaskDetailFragment( );
		Bundle bundle = new Bundle();
		if(task!=null){
			bundle.putString(KEY_TASK, GsonFactory.getConfiguredGson().toJson(task));
		}
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		mainActObj = getMainActivity();
		if(getArguments()!=null){
			if(getArguments().containsKey(KEY_TASK)){
				task = GsonFactory.getConfiguredGson().fromJson(getArguments().getString(KEY_TASK), TaskItem.class);
			}
		}
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		View view = inflater.inflate( R.layout.fragment_task_detail, null );
		return view;
	}
	
	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		fragmentTaskDetail = new FragmentTaskDetail( view );
		if(task!=null){
			fragmentTaskDetail.txtTitle.setText(task.getTitle());
			fragmentTaskDetail.txtNotes.setText(task.getNotes());
		}
	}
	
	@Override
	public void setTitleBar( TitleBar titleBar ) {
		titleBar.hideButtons();
		titleBar.setVisibility( View.VISIBLE );
		titleBar.setTitleTxt("Task Details");
		this.titleBar = titleBar;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		titleBar.setVisibility( View.VISIBLE );
	}

	@Override
	public void setCurrentFrag() {
		
	}

	@Override
	public void setFragmentId() {
		//mainActObj.setLastResumedFragmentID( ID );
		
	}

	
	
	@Override
	public void onBackPressed(){
		mainActObj.getSupportFragmentManager().popBackStack();
	}

	@Override
	public void onNetworkConnected() {
		GoogleHelper.getInstance().syncTasksRecusively();
		
	}

	@Override
	public void onNetworkDisconnected() {
		// TODO Auto-generated method stub
		
	}


}
