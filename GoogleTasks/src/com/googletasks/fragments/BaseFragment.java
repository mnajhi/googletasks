package com.googletasks.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.googletasks.MainActivity;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.ui.views.TitleBar;


public abstract class BaseFragment extends Fragment {
	
	protected final String TAG = "Fragment";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setFragmentId();
		setTitleBar( getTitleBar() );
	}
	
	protected TitleBar getTitleBar() {
		return getMainActivity().titleBar;
	}

	protected MainActivity getMainActivity() {
		return (MainActivity) getActivity();
	}
	
	public abstract void setCurrentFrag();
	
	public abstract void setFragmentId();
	public abstract void setTitleBar(TitleBar titleBar);
	public abstract void onNetworkConnected();
	public abstract void onNetworkDisconnected();
	
	abstract public void onBackPressed();

	public void onSyncComplete(){}

	public void onTaskUpdatedSuccess(TaskItem task){}

	public void onTaskUpdatedFailure(TaskItem task){}
	
}
