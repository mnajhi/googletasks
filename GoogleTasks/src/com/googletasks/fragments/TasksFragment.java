 package com.googletasks.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.google.common.base.Strings;
import com.googletasks.MainActivity;
import com.googletasks.database.DataBaseHandler;
import com.googletasks.google.GoogleHelper;
import com.googletasks.google.GoogleTasksListener;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.ui.adapters.ArrayListAdapter;
import com.googletasks.ui.viewbinders.TasksBinder;
import com.googletasks.ui.viewholders.FragmentTasks;
import com.googletasks.ui.views.TitleBar;
import com.najhi.googletaskstest.R;


public class TasksFragment extends BaseFragment implements OnClickListener{
	private static final String KEY_TASK_LIST_ID = "task_list_id";
	private MainActivity mainActObj;
	private TitleBar titleBar;
	private FragmentTasks fragmentTasks;
	private String taskListId;
	private boolean isDataLoaded = false;
	private ArrayListAdapter<TaskItem> adapter;
	
	public static TasksFragment newInstance(String taskListId) {
		TasksFragment fragment = new TasksFragment( );
		Bundle bundle = new Bundle();
		bundle.putString(KEY_TASK_LIST_ID, taskListId);
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		mainActObj = getMainActivity();
		if(getArguments()!=null){
			taskListId = getArguments().getString(KEY_TASK_LIST_ID);
		}
		adapter = new ArrayListAdapter<TaskItem>(mainActObj, new TasksBinder(mainActObj, this));
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
		View view = inflater.inflate( R.layout.fragment_tasks, null );
		return view;
	}
	
	private void populateAdapter(boolean usingDatabase) {
		
		if(!usingDatabase && !isDataLoaded){
			GoogleHelper.getInstance().getTasks(new GoogleTasksListener() {
				
				@Override
				public void onSuccess(ArrayList<TaskItem> tasks) {
					isDataLoaded = true;
					adapter.clearList();
					adapter.addAll(tasks);
					if(adapter.getCount()>0){
						fragmentTasks.lstTasks.setVisibility(View.VISIBLE);
						fragmentTasks.txtNoItem.setVisibility(View.GONE);
					} else {
						fragmentTasks.lstTasks.setVisibility(View.GONE);
						fragmentTasks.txtNoItem.setVisibility(View.VISIBLE);
					}
				}
				
				@Override
				public void onFailure(String errorMsg) {
					adapter.clearList();
					fragmentTasks.lstTasks.setVisibility(View.GONE);
					fragmentTasks.txtNoItem.setVisibility(View.VISIBLE);
				}
			}, taskListId);
		} else {
			adapter.clearList();
			adapter.addAll(DataBaseHandler.getInstance().getTasksN(taskListId));
			if(adapter.getCount()>0){
				fragmentTasks.lstTasks.setVisibility(View.VISIBLE);
				fragmentTasks.txtNoItem.setVisibility(View.GONE);
			} else {
				fragmentTasks.lstTasks.setVisibility(View.GONE);
				fragmentTasks.txtNoItem.setVisibility(View.VISIBLE);
			}
		}
		
		
		
	}
	
	@Override
	public void onViewCreated( View view, Bundle savedInstanceState ) {
		super.onViewCreated( view, savedInstanceState );
		fragmentTasks = new FragmentTasks( view );
		fragmentTasks.lstTasks.setAdapter(adapter);
		populateAdapter(false);

	}
	
	@Override
	public void setTitleBar( TitleBar titleBar ) {
		titleBar.hideButtons();
		titleBar.setVisibility( View.VISIBLE );
		titleBar.setTitleTxt("Tasks");
		titleBar.showRightButton(this, R.drawable.add);
		this.titleBar = titleBar;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		titleBar.setVisibility( View.VISIBLE );
	}

	@Override
	public void setCurrentFrag() {
		
	}

	@Override
	public void setFragmentId() {
		//mainActObj.setLastResumedFragmentID( ID );
		
	}

	@Override
	public void onClick( View v ) {
		switch ( v.getId() ) {
			case R.id.btnRight:
				TaskItem task = new TaskItem();
				task.setTaskListId(taskListId);
				mainActObj.addFragment(TaskAddFragment.newInstance(false,task), "TaskAddFragment");
				break;
			case R.id.relMain:
				TaskItem task0 = (TaskItem)v.getTag(R.string.task);
				mainActObj.addFragment(TaskDetailFragment.newInstance(task0), "TaskDetailFragment");
				break;
			case R.id.btnDelete:
				TaskItem task1 = (TaskItem)v.getTag(R.string.task);
				task1.setTaskListId(taskListId);
				GoogleHelper.getInstance().deleteTask(task1);
				populateAdapter(true);
				break;
			case R.id.btnEdit:
				TaskItem task2 = (TaskItem)v.getTag(R.string.task);
				task2.setTaskListId(taskListId);
				mainActObj.addFragment(TaskAddFragment.newInstance(true,task2), "TaskAddFragment");
				break;
		}
	}
	
	@Override
	public void onTaskUpdatedSuccess(TaskItem task) {
		Log.v(TAG, "onTaskUpdatedSuccess");
		ArrayList<TaskItem> tasks = (ArrayList<TaskItem>)adapter.getList();
		for(TaskItem t: tasks){
			if(Strings.isNullOrEmpty(t.getId())){
				if(t.getCreated() == task.getCreated()){
					t.copy(task);
					adapter.notifyDataSetChanged();
					break;
				}
			} else {
				if(t.getId().equals(task.getId())){
					t.copy(task);
					adapter.notifyDataSetChanged();
					break;
				}
			}
		}
	}
	
	@Override
	public void onTaskUpdatedFailure(TaskItem task) {
		Log.v(TAG, "onTaskUpdatedSuccess");
		ArrayList<TaskItem> tasks = (ArrayList<TaskItem>)adapter.getList();
		for(TaskItem t: tasks){
			if(Strings.isNullOrEmpty(t.getId())){
				if(t.getCreated() == task.getCreated()){
					t.setFailed(true);
					adapter.notifyDataSetChanged();
					break;
				}
			} else {
				if(t.getId().equals(task.getId())){
					t.setFailed(true);
					adapter.notifyDataSetChanged();
					break;
				}
			}
		}
	}
	
	@Override
	public void onBackPressed(){
		mainActObj.getSupportFragmentManager().popBackStack();
	}

	@Override
	public void onNetworkConnected() {
		GoogleHelper.getInstance().syncTasksRecusively();
		
	}

	@Override
	public void onNetworkDisconnected() {
		// TODO Auto-generated method stub
		
	}


}
