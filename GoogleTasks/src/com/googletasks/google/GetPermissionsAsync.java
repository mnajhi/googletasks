/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.googletasks.google;

import java.io.IOException;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

/**
 * This example shows how to fetch tokens if you are creating a foreground
 * task/activity and handle auth exceptions.
 */
public class GetPermissionsAsync extends AsyncTask<String, Void, HashMap<String, String>> {
	private static final String TAG = "GetPermissionsAsync";
	Context context_;
	String email_;
	GoogleLoginListener delegate_;
	GoogleHelper helper_;
	
	public GetPermissionsAsync(GoogleHelper helper, GoogleLoginListener delegate, Context context, String email) {
		this.context_ = context;
		this.email_ = email;
		this.delegate_ = delegate;
		this.helper_ = helper;
	}

	/**
	 * Get a authentication token if one is not available. If the error is not
	 * recoverable then it displays the error message on parent activity right
	 * away.
	 */
	private String fetchToken(String scope) throws IOException {
		try {
			return GoogleAuthUtil.getToken((Activity) context_, email_, scope);
		} catch (UserRecoverableAuthException userRecoverableException) {
			GoogleHelper.getInstance().handleException(
					userRecoverableException);
		} catch (GoogleAuthException fatalException) {
			onError("Unrecoverable error " + fatalException.getMessage(), fatalException);
		}
		return null;
	}

	@Override
	protected HashMap<String, String> doInBackground(String... params) {
		HashMap<String , String> tokenMap = new HashMap<String, String>();
		for(String scope : params){
			try {
				String token = fetchToken(scope.trim());
				if(token!=null){
					tokenMap.put(scope.trim(), token);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return tokenMap;
	}
	
	@Override
	protected void onPostExecute(HashMap<String, String> result) {
		super.onPostExecute(result);
		if(result.size()>0){
			helper_.setTokenMap_(result);
			if(delegate_!=null){
				delegate_.onSuccess(email_);
			}
		}
	}
	
	protected void onError(String msg, Exception e) {
        if (e != null) {
          Log.e(TAG, "Exception: ", e);
        }
        if(delegate_!=null){
        	delegate_.onFailure(msg, e);
        }
    }
}
