package com.googletasks.google.entities;

import com.google.gson.annotations.Expose;

public class Error {
//	private long code;
	@Expose
    private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
//	public long getCode() {
//		return code;
//	}
//	public void setCode(long code) {
//		this.code = code;
//	}
}
