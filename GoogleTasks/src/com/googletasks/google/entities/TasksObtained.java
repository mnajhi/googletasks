package com.googletasks.google.entities;

import java.util.ArrayList;

public class TasksObtained {

	private Error error;
	private ArrayList<TaskItem> items = new ArrayList<TaskItem>();

    public ArrayList<TaskItem> getItems() {
        return items == null? new ArrayList<TaskItem>(): items;
    }

    public void setItems(ArrayList<TaskItem> items) {
        this.items = items;
    }

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

}
