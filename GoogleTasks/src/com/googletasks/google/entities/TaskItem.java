
package com.googletasks.google.entities;


public class TaskItem {
	
    private String id;
    private String taskListId;
    private String title;
    private String updated;
    private String notes;
    private String status;
    private boolean isDirty;
    private boolean isDeleted;
    private long created;
    private boolean isFailed;
    
    public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isDirty() {
		return isDirty;
	}

	public void setDirty(boolean isDirty) {
		this.isDirty = isDirty;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getTaskListId() {
		return taskListId;
	}

	public void setTaskListId(String taskListId) {
		this.taskListId = taskListId;
	}
	
	public void copy(TaskItem task){
		this.id = task.getId();
	    this.taskListId = task.getTaskListId();
	    this.title = task.getTitle();
	    this.updated = task.getUpdated();
	    this.notes = task.getNotes();
	    this.status = task.getStatus();
	    this.isDirty = task.isDirty();
	    this.isDeleted = task.isDeleted();
	    this.created = task.getCreated();
	}

	public boolean isFailed() {
		return isFailed;
	}

	public void setFailed(boolean isFailed) {
		this.isFailed = isFailed;
	}

}
