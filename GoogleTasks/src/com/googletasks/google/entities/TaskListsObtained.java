
package com.googletasks.google.entities;

import java.util.ArrayList;
public class TaskListsObtained {

    private Error error;
    private ArrayList<TaskListItem> items;
    
    public ArrayList<TaskListItem> getItems() {
        return items == null? new ArrayList<TaskListItem>() : items;
    }

    public void setItems(ArrayList<TaskListItem> items) {
        this.items = items;
    }

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

}
