package com.googletasks.google;

import java.util.ArrayList;

import com.googletasks.google.entities.TaskItem;

public interface GoogleTasksListener {
	public void onSuccess(ArrayList<TaskItem> tasks);
	public void onFailure(String errorMsg);
}
