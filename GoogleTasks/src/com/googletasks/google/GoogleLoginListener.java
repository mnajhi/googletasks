package com.googletasks.google;

public interface GoogleLoginListener {
	public void onSuccess(String email);
	public void onFailure(String message, Exception ex);
}
