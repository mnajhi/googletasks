package com.googletasks.google;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.RetrofitError;
import retrofit.client.Response;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.common.base.Strings;
import com.googletasks.MainActivity;
import com.googletasks.database.DataBaseHandler;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.google.entities.TaskItemRequestBody;
import com.googletasks.google.entities.TaskListsObtained;
import com.googletasks.google.entities.TasksObtained;
import com.googletasks.google.webrestapi.CustomWebResponse;
import com.googletasks.google.webrestapi.WebServiceFactory;

public class GoogleHelper {
	private static final String TAG = "GoogleLoginHelper";
	private static final String SCOPE_TASKS = "oauth2:https://www.googleapis.com/auth/tasks";
	static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
	static final int REQUEST_CODE_RECOVER_FROM_AUTH_ERROR = 1001;
	static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1002;

	private final int LOGIN = 3;

	private static GoogleHelper instance_;
	private Context context_;
	private String email_;
	private GoogleLoginListener loginDelegate_;
	
	private HashMap<String, String> tokenMap_;
	private int getType = 0;
	private int retryCount = 3;
	
	public static GoogleHelper getInstance(){
		if(instance_==null){
			instance_ = new GoogleHelper();
		}
		return instance_;
	}

	public void init(Context context){
		context_ = context;
	}

	public void login(GoogleLoginListener delegate){
		this.loginDelegate_ = delegate;
		getType = LOGIN;
		int statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context_);
		if (statusCode == ConnectionResult.SUCCESS) {
			if(email_==null){
				pickUserAccount();
			} else {
				if(isDeviceOnline()){
					new GetPermissionsAsync(this, loginDelegate_, context_, email_).execute(SCOPE_TASKS);
				} else {
					loginDelegate_.onFailure("No network connection available", null);
				}
			}
			
		} else if (GooglePlayServicesUtil.isUserRecoverableError(statusCode)) {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode, (Activity)context_, 0 /* request code not used */);
			dialog.show();
		} else {
			Toast.makeText(context_, "Unrecoverable Play Services error", Toast.LENGTH_SHORT).show();
			if(loginDelegate_!=null){
				loginDelegate_.onFailure("Unrecoverable Play Services error",null);
			}
		}
	}

	public void getTasksList(final GoogleTaskListsListener delegate){
		if(isDeviceOnline() ){
			String token = "";
			if(getTokenMap_()!=null && getTokenMap_().containsKey(SCOPE_TASKS)){
				token = getTokenMap_().get(SCOPE_TASKS);
			}
			if(!Strings.isNullOrEmpty(token)){
				WebServiceFactory.getInstance().getLists(
						token,//access_token, 
						new CustomWebResponse<TaskListsObtained>(context_) {
							@Override
							public void success(TaskListsObtained arg0, Response arg1) {
								DataBaseHandler.getInstance().bulkAddTaskListsInTable(arg0.getItems());
								delegate.onSuccess(arg0.getItems());
							}
							
							@Override
							public void failure(RetrofitError cause) {
								super.failure(cause);
								delegate.onFailure("Error");
							}
						});
			} else {
				delegate.onSuccess(DataBaseHandler.getInstance().getTaskListsN());
			}
		} else {
			delegate.onSuccess(DataBaseHandler.getInstance().getTaskListsN());
		}
	}
	
	public void insertTask(TaskItem task){
		task.setDirty(true);
		task.setCreated(System.currentTimeMillis());
		DataBaseHandler.getInstance().addTaskInTable(task);
		syncTasksRecusively();
	}
	
	public void updateTask(TaskItem task){
		if(!Strings.isNullOrEmpty(task.getId())){
			task.setDirty(true);
			DataBaseHandler.getInstance().updateTask(task);
		} else {
			DataBaseHandler.getInstance().updateTaskWithCreatedDate(task);
		}
		syncTasksRecusively();
	}
	
	public void deleteTask(TaskItem task){
		if(!Strings.isNullOrEmpty(task.getId())){
			task.setDirty(true);
			task.setDeleted(true);
			DataBaseHandler.getInstance().updateTask(task);
		} else {
			DataBaseHandler.getInstance().deleteTaskFromTableWithCreatedDate(task.getCreated());
		}
		syncTasksRecusively();
	}
	
	public void getTasks(final GoogleTasksListener delegate, final String taskListId){
		if(isDeviceOnline()){
			String token = "";
			if(getTokenMap_()!=null && getTokenMap_().containsKey(SCOPE_TASKS)){
				token = getTokenMap_().get(SCOPE_TASKS);
			}
			if(!Strings.isNullOrEmpty(token)){
				WebServiceFactory.getInstance().getTaskItems(
					taskListId, 
					token, 
					new CustomWebResponse<TasksObtained>(context_) {
						@Override
						public void success(TasksObtained arg0, Response arg1) {
							DataBaseHandler.getInstance().bulkAddTasksInTable(arg0.getItems(), taskListId);
							ArrayList<TaskItem> items = DataBaseHandler.getInstance().getTasksN(taskListId);
							delegate.onSuccess(items);
						}
						
						@Override
						public void failure(RetrofitError cause) {
							super.failure(cause);
							delegate.onFailure("Error");
						}
					});
			} else {
				delegate.onSuccess(DataBaseHandler.getInstance().getTasksN(taskListId));
			}
		} else {
			delegate.onSuccess(DataBaseHandler.getInstance().getTasksN(taskListId));
		}
	}
	
	
	public void syncTasksRecusively(){
		Log.v(TAG, "syncTasksRecusively");
		((MainActivity)context_).getTitleBar().showProgress();
		final TaskItem task = DataBaseHandler.getInstance().getDirtyTaskN();
		if(task==null || !isDeviceOnline()){
			((MainActivity)context_).getTitleBar().hideProgress();
			return;
		}
		
		String token = "";
		if(getTokenMap_()!=null && getTokenMap_().containsKey(SCOPE_TASKS)){
			token = getTokenMap_().get(SCOPE_TASKS);
		}
		if(!Strings.isNullOrEmpty(token)){
			TaskItemRequestBody body = new TaskItemRequestBody();
			body.setTitle(task.getTitle());
			body.setNotes(task.getNotes());
			
			//If Task Id is empty that mean have to insert in Google Tasks
			if(Strings.isNullOrEmpty(task.getId())){
				
				WebServiceFactory.getInstance().insertTask(
						task.getTaskListId(),//tasklist_id, 
						body, 
						token, 
						new CustomWebResponse<TaskItem>(context_) {
							@Override
							public void success(TaskItem arg0, Response arg1) {
								arg0.setCreated(task.getCreated());
								arg0.setTaskListId(task.getTaskListId());
								DataBaseHandler.getInstance().updateTaskWithCreatedDate(arg0);
								retryCount = 3;
								((MainActivity)context_).onTaskUpdatedSuccess(arg0);
								syncTasksRecusively();
							}
							
							@Override
							public void failure(RetrofitError cause) {
								super.failure(cause);
								if(retryCount!=0){
									retryCount--;
									syncTasksRecusively();
								} else {
									retryCount = 3;
									((MainActivity)context_).onTaskUpdatedFailure(task);
									((MainActivity)context_).getTitleBar().hideProgress();
								}
							}
						});
			} else if(task.isDirty()){
				if(task.isDeleted()){
					WebServiceFactory.getInstance().deleteTask(
							task.getTaskListId(),//tasklist_id, 
							task.getId(),//task_id, 
							token, 
							new CustomWebResponse<TaskItem>(context_) {
								@Override
								public void success(TaskItem arg0, Response arg1) {
									Log.e("Response", "Response");
									DataBaseHandler.getInstance().deleteTaskFromTableWithId(task.getId());
									retryCount = 3;
									syncTasksRecusively();
								}
								
								public void failure(RetrofitError cause) {
									super.failure(cause);
									if(retryCount!=0){
										retryCount--;
										syncTasksRecusively();
									} else {
										retryCount = 3;
										((MainActivity)context_).getTitleBar().hideProgress();
									}
								};
							});
				} else {
					WebServiceFactory.getInstance().updateTask(
							task.getTaskListId(),//tasklist_id, 
							body,
							task.getId(),
							token, 
							new CustomWebResponse<TaskItem>(context_) {
								
								@Override
								public void success(TaskItem arg0, Response arg1) {
									arg0.setTaskListId(task.getTaskListId());
									DataBaseHandler.getInstance().updateTask(arg0);
									retryCount = 3;
									((MainActivity)context_).onTaskUpdatedSuccess(arg0);
									syncTasksRecusively();
								}
								
								@Override
								public void failure(RetrofitError cause) {
									super.failure(cause);
									if(retryCount!=0){
										retryCount--;
										syncTasksRecusively();
									} else {
										retryCount = 3;
										((MainActivity)context_).onTaskUpdatedFailure(task);
										((MainActivity)context_).getTitleBar().hideProgress();
									}
								}
							});
				}
			}
		} else {
			retryCount = 3;
			((MainActivity)context_).getTitleBar().hideProgress();
		}
	}

	public void onDestroy(){
		context_ = null;
		email_ = null;
		loginDelegate_ = null;
		instance_ = null;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
			if (resultCode == Activity.RESULT_OK) {
				email_ = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				if(isDeviceOnline()){
					new GetPermissionsAsync(this, loginDelegate_, context_, email_).execute( SCOPE_TASKS);
				} else {
					loginDelegate_.onFailure("No network connection available", null);
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				loginDelegate_.onFailure("Login Failed", null);
			}
		} else if ((requestCode == REQUEST_CODE_RECOVER_FROM_AUTH_ERROR ||
				requestCode == REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR)) {
			if(resultCode== Activity.RESULT_OK){
				handleAuthorizeResult(resultCode, data);
			} else if (resultCode == Activity.RESULT_CANCELED) {
				loginDelegate_.onFailure("You must grant permisssion to access Application", null);
			}
			return;
		}
	}
	
	private void handleAuthorizeResult(int resultCode, Intent data) {
		if (data == null) {
			Toast.makeText(context_, "Unknown error, click the button again", Toast.LENGTH_LONG).show();
			return;
		}
		if (resultCode == Activity.RESULT_OK) {
			Log.i(TAG, "Retrying");
			if(getType == LOGIN){
				new GetPermissionsAsync(this, loginDelegate_, context_, email_).execute( SCOPE_TASKS);
			} 
			return;
		}
		if (resultCode == Activity.RESULT_CANCELED) {
			Toast.makeText(context_, "User rejected authorization.", Toast.LENGTH_LONG).show();
			return;
		}
		Toast.makeText(context_,"Unknown error, click the button again", Toast.LENGTH_LONG).show();
	}

	public void handleException(final Exception e) {
		((Activity)context_).runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (e instanceof GooglePlayServicesAvailabilityException) {
					// The Google Play services APK is old, disabled, or not present.
					// Show a dialog created by Google Play services that allows
					// the user to update the APK
					int statusCode = ((GooglePlayServicesAvailabilityException)e)
							.getConnectionStatusCode();
					Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode,
							(Activity)context_,
							REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
					dialog.show();
				} else if (e instanceof UserRecoverableAuthException) {
					// Unable to authenticate, such as when the user has not yet granted
					// the app access to the account, but the user can fix this.
					// Forward the user to an activity in Google Play services.
					Intent intent = ((UserRecoverableAuthException)e).getIntent();
					((Activity)context_).startActivityForResult(intent,
							REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
				}
			}
		});
	}

	private boolean isDeviceOnline() {
		ConnectivityManager connMgr = (ConnectivityManager)context_.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	private void pickUserAccount() {
		String[] accountTypes = new String[]{"com.google"};
		Intent intent = AccountPicker.newChooseAccountIntent(null, null, accountTypes, false, null, null, null, null);
		((Activity)context_).startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
	}

	public HashMap<String, String> getTokenMap_() {
		return tokenMap_;
	}

	public void setTokenMap_(HashMap<String, String> tokenMap_) {
		this.tokenMap_ = tokenMap_;
	}

}
