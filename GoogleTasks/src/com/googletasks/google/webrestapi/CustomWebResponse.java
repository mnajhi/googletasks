package com.googletasks.google.webrestapi;

import java.net.SocketTimeoutException;

import retrofit.Callback;
import retrofit.RetrofitError;
import android.content.Context;
import android.widget.Toast;

import com.googletasks.MainActivity;

public abstract class CustomWebResponse<T> implements Callback<T>{
	
	private Context context;

	public CustomWebResponse(Context context){
		this.context = context;
		
	}

	@Override
	public void failure( RetrofitError cause ) {
		if(context instanceof MainActivity){
//			((MainActivity)context).onLoadingFinished();
		}
		
		if (cause.getCause() instanceof SocketTimeoutException) {
          Toast.makeText(context,  "Connection timeout", Toast.LENGTH_LONG ).show();
      } else {
    	  Toast.makeText(context,  "Connection unreachable", Toast.LENGTH_LONG ).show();
      }
	}
}
