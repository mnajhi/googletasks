package com.googletasks.google.webrestapi;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

import com.googletasks.google.entities.TaskItem;
import com.googletasks.google.entities.TaskItemRequestBody;
import com.googletasks.google.entities.TaskListsObtained;
import com.googletasks.google.entities.TasksObtained;

public interface WebService {
//	@Headers({"Content-type: application/json; charset=\"utf-8\""})
	@GET("/users/@me/lists")
	public void getLists(
			@Query("access_token") String access_token,
			CustomWebResponse<TaskListsObtained> response);

	@GET("/lists/{tasklist_id}/tasks")
	public void getTaskItems(
			@Path("tasklist_id") String tasklist_id,
			@Query("access_token") String access_token,
			CustomWebResponse<TasksObtained> response);
	
	@POST("/lists/{tasklist_id}/tasks")
	public void insertTask(
			@Path("tasklist_id") String tasklist_id,
			@Body TaskItemRequestBody body,
			@Query("access_token") String access_token,
			CustomWebResponse<TaskItem> response);
	
	@PATCH("/lists/{tasklist_id}/tasks/{task_id}")
	public void updateTask(
			@Path("tasklist_id") String tasklist_id,
			@Body TaskItemRequestBody body,
			@Path("task_id") String task_id,
			@Query("access_token") String access_token,
			CustomWebResponse<TaskItem> response);
	
	@DELETE("/lists/{tasklist_id}/tasks/{task_id}")
	public void deleteTask(
			@Path("tasklist_id") String tasklist_id,
			@Path("task_id") String task_id,
			@Query("access_token") String access_token,
			CustomWebResponse<TaskItem> response);

}