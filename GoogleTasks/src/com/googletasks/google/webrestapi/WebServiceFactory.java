package com.googletasks.google.webrestapi;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import android.util.Log;

import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.OkHttpClient;

public class WebServiceFactory {

	private static final String TAG = "Retrofit"; 
	public static WebService service;
	private static final String SERVER_URL = "https://www.googleapis.com/tasks/v1/";

	public static WebService getInstance() {
		if (service == null) {

			OkHttpClient name = new OkHttpClient();
			name.setConnectionPool(new ConnectionPool(5, 15 * 1000));

			RestAdapter restAdapter = new RestAdapter.Builder()
					.setConverter(new GsonConverter(GsonFactory.getConfiguredGson()))
					.setLog(new RestAdapter.Log() {
						@Override
						public void log(String arg0) {
							Log.v(TAG, arg0);
						}
					})
					.setLogLevel(LogLevel.FULL)
					.setEndpoint(SERVER_URL)
					.setClient(new OkClient(name))
					.build();

			service = restAdapter.create(WebService.class);
		}

		return service;

	}

}
