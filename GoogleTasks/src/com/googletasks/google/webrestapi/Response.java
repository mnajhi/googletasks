package com.googletasks.google.webrestapi;


public class Response<T> {
	
	private T Result;
	public String Response;
	private String Message;

	private String totalVenues;
	
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	/**
	 * @return the result
	 */
	public T getResult() {
		return Result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult( T result ) {
		Result = result;
	}
	/**
	 * @return the totalVenues
	 */
	public String getTotalVenues() {
		return totalVenues;
	}
	/**
	 * @param totalVenues the totalVenues to set
	 */
	public void setTotalVenues( String totalVenues ) {
		this.totalVenues = totalVenues;
	}
	
}
