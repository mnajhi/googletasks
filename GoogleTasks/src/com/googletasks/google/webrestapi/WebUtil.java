package com.googletasks.google.webrestapi;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class WebUtil {
	public static TypedString getTypedString(String string){
		if(string==null)
			string = "";
		TypedString tString =new TypedString(string);
		return tString;
	}
	
	public static TypedFile getTypedFile(String filePath){
		if(filePath==null){
			filePath = "";
		}
		File file = new File(filePath);
		TypedFile tFile =new TypedFile("image/jpeg",file);
		return tFile;
	}
	
	public static TypedFile getTypedFile(File file){
		TypedFile tFile =new TypedFile("image/jpeg",file);
		return tFile;
	}
	
	public static MultipartTypedOutput getMultipartTypedOutput(HashMap<String, Object> paramsMap){
		MultipartTypedOutput multipartTypedOutput = new MultipartTypedOutput();
		for(Map.Entry<String, Object> entry : paramsMap.entrySet()){
			if(entry.getValue() instanceof TypedFile){
			    multipartTypedOutput.addPart(entry.getKey(), (TypedFile)entry.getValue());
			}
			else{
			    multipartTypedOutput.addPart(entry.getKey(), new TypedString(entry.getValue().toString()));
			}
		}
		return multipartTypedOutput;
	}
	
	public static String generateMD5HashString(String message) {
		String digest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(message.getBytes("UTF-8"));

			// converting byte array to Hexadecimal String
			StringBuilder sb = new StringBuilder(2 * hash.length);
			for (byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}

			digest = sb.toString();

		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		}
		return digest;
	}
	
}
