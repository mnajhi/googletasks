package com.googletasks.google;

import java.util.ArrayList;

import com.googletasks.google.entities.TaskListItem;

public interface GoogleTaskListsListener {
	public void onSuccess(ArrayList<TaskListItem> taskLists);
	public void onFailure(String errorMsg);
}
