package com.googletasks;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.googletasks.database.DataBaseHandler;
import com.googletasks.fragments.BaseFragment;
import com.googletasks.fragments.TaskListsFragment;
import com.googletasks.google.GoogleHelper;
import com.googletasks.google.GoogleLoginListener;
import com.googletasks.google.entities.TaskItem;
import com.googletasks.helpers.GPSTracker;
import com.googletasks.ui.views.TitleBar;
import com.najhi.googletaskstest.R;

public class MainActivity extends FragmentActivity {
	
	public TitleBar titleBar;
	private Handler handler = new Handler();
	public FrameLayout frame;
	private TextView txtOffline;
	
	private NetworkStateReceiver networkStateReceiver;
	
	private GPSTracker locationHelper;
	
	public class NetworkStateReceiver extends BroadcastReceiver {

		@Override
		public void onReceive( final Context context, final Intent intent ) {
			if ( intent.getExtras() != null ) {
				final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
				final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
				if ( ni != null && ni.isConnected() ) {
					Log.e("Network", "Connected");
					Fragment frag =getActiveFragment(); 
					if(frag!=null){
						((BaseFragment)frag).onNetworkConnected();
					}
					runOnUiThread(new Runnable() {
						public void run() {
							txtOffline.setVisibility(View.GONE);	
						}
					});
					
				}else{
					Log.e("Network", "Disconnected");
					Fragment frag =getActiveFragment(); 
					if(frag!=null){
						((BaseFragment)frag).onNetworkDisconnected();;
					}
					runOnUiThread(new Runnable() {
						public void run() {
							txtOffline.setVisibility(View.VISIBLE);	
						}
					});
					
				}
			}
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        DataBaseHandler.getInstance().init(this);
        GoogleHelper.getInstance().init(this);
        
        frame = (FrameLayout) findViewById(R.id.content_frame);
        titleBar = (TitleBar) findViewById(R.id.titleBar);
        txtOffline = (TextView) findViewById(R.id.txtOffline);
        
        GoogleHelper.getInstance().login(new GoogleLoginListener() {
			
			@Override
			public void onSuccess(String email) {
				emptyBackStack();
				addFragment(TaskListsFragment.newInstance(), "TaskListsFragment");
			}
			
			@Override
			public void onFailure(String message, Exception ex) {
				emptyBackStack();
				addFragment(TaskListsFragment.newInstance(), "TaskListsFragment");
			}
		});
        
        logTokens();
        networkStateReceiver = new NetworkStateReceiver();
        setLocationHelper(new GPSTracker(MainActivity.this));
    }
    
    public TitleBar getTitleBar() {
		titleBar.setVisibility(View.VISIBLE);
		return titleBar;
	}
    
    @Override
    protected void onPause() {
    	unregisterReceiver(networkStateReceiver);
    	super.onPause();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	registerReceiver( networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION ) );
    }
    
    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
    	GoogleHelper.getInstance().onActivityResult(arg0, arg1, arg2);
    	super.onActivityResult(arg0, arg1, arg2);
    }
    
    public void addFragment(final Fragment fragment, final String strTag) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				try {
					FragmentTransaction fragmentTransaction = getSupportFragmentManager()
							.beginTransaction();
					fragmentTransaction.setCustomAnimations(
							R.anim.push_right_in,
							R.anim.push_right_out,
							R.anim.push_left_in,
							R.anim.push_left_out);

					fragmentTransaction.replace(R.id.content_frame, fragment, strTag);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}
		});
	}
    
    public void emptyBackStack() {
		if (getSupportFragmentManager() == null)
			return;
		if (getSupportFragmentManager().getBackStackEntryCount() <= 0)
			return;
		BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
				0);

		if (entry != null) {

			getSupportFragmentManager().popBackStack(entry.getId(),
					FragmentManager.POP_BACK_STACK_INCLUSIVE);

		}
	}
    
    @Override
	public void onBackPressed() {

		if (this.getSupportFragmentManager().getBackStackEntryCount() > 1) {
			getActiveFragment().onBackPressed();
		}

		else if (this.getSupportFragmentManager().getBackStackEntryCount() < 2) {
			getActiveFragment().onBackPressed();
			showQuitDialog();
		}

	}
    
    public void onTaskUpdatedSuccess(TaskItem task){
    	if (this.getSupportFragmentManager().getBackStackEntryCount() > 0) {
			getActiveFragment().onTaskUpdatedSuccess(task);
		}
    }
    
    public void onTaskUpdatedFailure(TaskItem task){
    	if (this.getSupportFragmentManager().getBackStackEntryCount() > 0) {
			getActiveFragment().onTaskUpdatedFailure(task);
		}
    }
    
    public void onSyncComplete(){
    	if (this.getSupportFragmentManager().getBackStackEntryCount() > 0) {
			getActiveFragment().onSyncComplete();
		}
    }
	
	public BaseFragment getActiveFragment(){
	    FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
	    List<Fragment> fragments = fragmentManager.getFragments();
	    if(fragments!=null){
		    for(Fragment fragment : fragments){
		        if(fragment != null && fragment.isVisible())
		            return (BaseFragment) fragment;
		    }
	    }
	    return null;
	}

    
    public void showQuitDialog() {

		AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
				.create();
		dialog.setTitle("Attention!");
		dialog.setMessage("Are you sure you want to quit application?");
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						//Do nothing
					}
				});

		dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});

		dialog.show();

	}

    
    private void logTokens() {

		// Add code to print out the key hash
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getApplicationContext().getPackageName(),
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA-1");
				md.update(signature.toByteArray());
				Log.d("SHA1-KeyHash:::",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));

				md = MessageDigest.getInstance("MD5");
				md.update(signature.toByteArray());
				Log.d("MD5-KeyHash:::",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));

				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("SHA-Hex-From-KeyHash:::", bytesToHex(md.digest()));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
	}

	final protected static char[] hexArray = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	public boolean isDeviceOnline() {
		ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}
    
    @Override
    protected void onDestroy() {
    	GoogleHelper.getInstance().onDestroy();
    	super.onDestroy();
    }

	public GPSTracker getLocationHelper() {
		return locationHelper;
	}

	public void setLocationHelper(GPSTracker locationHelper) {
		this.locationHelper = locationHelper;
	}
}
