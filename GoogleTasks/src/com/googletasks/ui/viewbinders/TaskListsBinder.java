package com.googletasks.ui.viewbinders;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;

import com.googletasks.google.entities.TaskListItem;
import com.googletasks.ui.viewholders.BaseViewHolder;
import com.googletasks.ui.viewholders.ListItem;
import com.najhi.googletaskstest.R;

public class TaskListsBinder extends ViewBinder<TaskListItem> {

	private Context cntxt;
	private OnClickListener clickListener;
	
	public TaskListsBinder(Context cntxt,OnClickListener clickListener) {
		super(R.layout.list_item);
		this.cntxt = cntxt;
		this.clickListener = clickListener;		
	}
	
	@Override
	public BaseViewHolder createViewHolder(View view) {
		ListItem holder = new ListItem(view);
		return holder;
	}

	@Override
	public void bindView(TaskListItem entity, final int position, int grpPosition, View view, Activity activity) {
		ListItem item = (ListItem)view.getTag();
		if(entity==null){
			return;
		}
		
		item.relMain.setTag(R.string.task_list_id, entity.getId());
		item.lnrAttributes.setVisibility(View.GONE);
		item.txtTitle.setText(entity.getTitle());
		item.relMain.setOnClickListener(clickListener);
		if(position%2==0){
			item.relMain.setBackgroundColor(Color.WHITE);
		} else {
			item.relMain.setBackgroundColor(Color.LTGRAY);
		}
		
	}
}
