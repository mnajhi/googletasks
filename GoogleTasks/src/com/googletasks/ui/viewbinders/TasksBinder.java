package com.googletasks.ui.viewbinders;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;

import com.googletasks.google.entities.TaskItem;
import com.googletasks.ui.viewholders.BaseViewHolder;
import com.googletasks.ui.viewholders.ListItem;
import com.najhi.googletaskstest.R;

public class TasksBinder extends ViewBinder<TaskItem> {

	private Context cntxt;
	private OnClickListener clickListener;
	
	public TasksBinder(Context cntxt,OnClickListener clickListener) {
		super(R.layout.list_item);
		this.cntxt = cntxt;
		this.clickListener = clickListener;		
	}
	
	@Override
	public BaseViewHolder createViewHolder(View view) {
		ListItem holder = new ListItem(view);
		return holder;
	}

	@Override
	public void bindView(TaskItem entity, final int position, int grpPosition, View view, Activity activity) {
		ListItem item = (ListItem)view.getTag();
		if(entity==null){
			return;
		}
		
		item.txtTitle.setText(entity.getTitle());
		item.relMain.setOnClickListener(clickListener);
		item.btnEdit.setOnClickListener(clickListener);
		item.btnDelete.setOnClickListener(clickListener);
		item.btnEdit.setTag(R.string.task,entity);
		item.btnDelete.setTag(R.string.task,entity);
		item.relMain.setTag(R.string.task,entity);
		if(entity.isFailed()){
			item.relMain.setBackgroundColor(Color.parseColor("#ff9898"));
		} else if(entity.isDirty()){
			item.relMain.setBackgroundColor(Color.parseColor("#ffb569"));
		} else {
			item.relMain.setBackgroundColor(Color.parseColor("#98ff9a"));
		}
		
	}
}
