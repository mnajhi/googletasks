package com.googletasks.ui.viewholders;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.najhi.googletaskstest.R;

public class FragmentAddTasks extends BaseViewHolder {

	public EditText edtTitle;
	public EditText edtNotes;
	public Button btnCreate;

	public FragmentAddTasks(View rootView) {
		edtTitle = (EditText) rootView.findViewById(R.id.edtTitle);
		edtNotes = (EditText) rootView.findViewById(R.id.edtNotes);
		btnCreate = (Button) rootView.findViewById(R.id.btnCreate);
	}
}
