package com.googletasks.ui.viewholders;

import android.view.View;
import android.widget.TextView;

import com.najhi.googletaskstest.R;

public class FragmentTaskDetail extends BaseViewHolder {

	public TextView txtTitle;
	public TextView txtNotes;

	public FragmentTaskDetail(View rootView) {
		txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
		txtNotes = (TextView) rootView.findViewById(R.id.txtNotes);
	}
}
