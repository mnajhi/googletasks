package com.googletasks.ui.viewholders;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.najhi.googletaskstest.R;

public class FragmentTasks extends BaseViewHolder {

	public TextView txtNoItem;
	public ListView lstTasks;
	
	public FragmentTasks(View rootView) {
		lstTasks =  ( ListView ) rootView.findViewById(R.id.lstTasks);
		txtNoItem =  ( TextView ) rootView.findViewById(R.id.txtNoItem);
	}
}
