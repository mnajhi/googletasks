package com.googletasks.ui.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.najhi.googletaskstest.R;

public class ListItem extends BaseViewHolder{
	public RelativeLayout relMain;
	public TextView txtTitle;
	public LinearLayout lnrAttributes;
	public ImageView btnEdit;
	public ImageView btnDelete;

	public ListItem(View rootView) {
		relMain = ( RelativeLayout ) rootView.findViewById(R.id.relMain);
		txtTitle = ( TextView ) rootView.findViewById(R.id.txtTitle);
		lnrAttributes = ( LinearLayout ) relMain.findViewById(R.id.lnrAttributes);
		btnEdit = ( ImageView ) relMain.findViewById(R.id.btnEdit);
		btnDelete = ( ImageView ) relMain.findViewById(R.id.btnDelete);
	}
}
