package com.googletasks.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.najhi.googletaskstest.R;

public class TitleBar extends RelativeLayout {

	private TextView txtTitle;
	private ImageView btnRight;
	private ProgressBar prgSync;
	
	
	public TitleBar( Context context ) {
		super( context );
		initLayout( context );
	}

	public TitleBar( Context context, AttributeSet attrs ) {
		super( context, attrs );
		initLayout( context );
		if ( attrs != null )
			initAttrs( context, attrs );
	}

	public TitleBar( Context context, AttributeSet attrs, int defStyle ) {
		super( context, attrs, defStyle );
		initLayout( context );
		if ( attrs != null )
			initAttrs( context, attrs );
	}

	private void initAttrs( Context context, AttributeSet attrs ) {
	}

	private void initLayout( Context context ) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		try {
			inflater.inflate( R.layout.view_title_bar, this );
		} catch ( Exception e ) {
			Log.e( "view-error", e.toString() );
		}
		bindViews();

	}
	
	private void bindViews() {
		btnRight = (ImageView) findViewById( R.id.btnRight );
		txtTitle = (TextView) findViewById( R.id.txtTitle );
		prgSync = (ProgressBar) findViewById(R.id.prgSync);
		
	}
	
	public void showProgress(){
		prgSync.setVisibility(View.VISIBLE);
	}
	
	public void hideProgress(){
		prgSync.setVisibility(View.GONE);
	}

	public ImageView getRightButton() {
		return btnRight;
	}

	public void setTitleTxt(String strTxtTitle) {
		txtTitle.setText( strTxtTitle );
	}

	public String getTitleTxt() {
		return (String) txtTitle.getText();
	}

	public void setOnRightClickListener( View.OnClickListener listener ) {
		btnRight.setOnClickListener( listener );
	}

	public void setOnTitleClickListener( View.OnClickListener listener ) {
		txtTitle.setOnClickListener( listener );
	}

	public void hideButtons() {
		if ( getVisibility() == View.GONE || getVisibility() == View.INVISIBLE )
			setVisibility( View.VISIBLE );
		btnRight.setVisibility( View.GONE );
	}

	public void showRightButton(View.OnClickListener listener,int imgResource) {
		btnRight.setVisibility(View.VISIBLE);
		btnRight.setImageResource( imgResource );
		btnRight.setOnClickListener(listener);
	}

}
