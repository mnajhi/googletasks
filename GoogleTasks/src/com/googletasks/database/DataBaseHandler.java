package com.googletasks.database;

import java.util.ArrayList;

import com.googletasks.google.entities.TaskItem;
import com.googletasks.google.entities.TaskListItem;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;



public class DataBaseHandler {

	private SQLiteDatabase database;
	private SqliteOpenHelper dbHelper;
	private boolean isDataBaseOpen = false;

	private final String[] taskListsColumns = {
			SqliteOpenHelper.taskListId,
			SqliteOpenHelper.title
	};
	
	private final String[] tasksColumns = {
			SqliteOpenHelper.taskId,
		    SqliteOpenHelper.taskListId,
		    SqliteOpenHelper.title,
		    SqliteOpenHelper.updated,
		    SqliteOpenHelper.notes,
		    SqliteOpenHelper.status,
		    SqliteOpenHelper.isDirty,
		    SqliteOpenHelper.isDeleted,
		    SqliteOpenHelper.created
	};
	
	private static DataBaseHandler instance = null;

	public static DataBaseHandler getInstance() {
		if (instance == null) {
			instance = new DataBaseHandler();
		}
		return instance;
	}

	/**
	 * Setting current instance to null. when getInstance will be called new
	 * Object will be returned
	 */
	public void clearInstance() {
		instance = null;
		SqliteOpenHelper.clearInstance();
	}
	
	public void init(Context context){
		dbHelper =  SqliteOpenHelper.getInstance(context);
	}

	private void open() throws SQLException {
		if(!isDataBaseOpen){
			database = dbHelper.getWritableDatabase();
			isDataBaseOpen = true;
		}
	}

	private void close() {
		if(isDataBaseOpen){
			dbHelper.close();
			isDataBaseOpen = false;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                                     Task Lists                                                      //
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public synchronized void bulkAddTaskListsInTable(ArrayList<TaskListItem> taskListsArray){
		if(taskListsArray==null){
			return;
		}
		deleteTaskListsFromTable();
		open();
		String tableName = SqliteOpenHelper.Table_Task_Lists;
		
		final String INSERT_QUERY = createInsert(tableName, taskListsColumns);
		final SQLiteStatement statement = database.compileStatement(INSERT_QUERY);
		database.beginTransaction();
		int columnIndex = 1;
		try {
	        for(TaskListItem taskLists : taskListsArray){
	        	statement.clearBindings();
	            columnIndex = 1;
	            
	            String id = taskLists.getId();
	        	String title = taskLists.getTitle();
	            
	            if(id!=null){statement.bindString(columnIndex++,id);}else{columnIndex++;}
	            if(title!=null){statement.bindString(columnIndex++,title);}else{columnIndex++;}
	            
	            // rest of bindings
	            statement.execute(); //or executeInsert() if id is needed
	        }
	        database.setTransactionSuccessful();
	    } finally {
	        database.endTransaction();
	        close();
	    }
		
	}
	
	public synchronized ArrayList<TaskListItem> getTaskListsN(){
		ArrayList<TaskListItem> taskListsArray = new ArrayList<TaskListItem>();
		open();
		TaskListItem taskListItem = null;
		String tableName = SqliteOpenHelper.Table_Task_Lists;
		String selectQuery = "SELECT  * FROM " + tableName;
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst();
			do{
				taskListItem = new TaskListItem();
				taskListItem.setId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskListId)));
				taskListItem.setTitle(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.title)));
				taskListsArray.add(taskListItem);
			}while(cursor.moveToNext());
		}
		cursor.close();
		close();
		
		return taskListsArray;
	}
	
	public synchronized int getCampaignsCount(){
		open();
		String tableName = SqliteOpenHelper.Table_Task_Lists;
		Cursor mCount= database.rawQuery("select count(*) from " + tableName, null);
		mCount.moveToFirst();
		int count= mCount.getInt(0);
		mCount.close();
		close();
		return count;
	}
	
	public synchronized void deleteTaskListsFromTable(){
		String tableName = SqliteOpenHelper.Table_Task_Lists;
		open();
		database.execSQL("DELETE FROM "+tableName);
		close();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                                         Tasks                                                       //
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public synchronized void bulkAddTasksInTable(ArrayList<TaskItem> tasksArray, String taskListId){
		if(tasksArray==null){
			return;
		}
		deleteTasksFromTable(taskListId);
		open();
		String tableName = SqliteOpenHelper.Table_Tasks;
		
		final String INSERT_QUERY = createInsert(tableName, tasksColumns);
		final SQLiteStatement statement = database.compileStatement(INSERT_QUERY);
		database.beginTransaction();
		int columnIndex = 1;
		try {
	        for(TaskItem task : tasksArray){
	        	statement.clearBindings();
	            columnIndex = 1;
	            
	            String id = task.getId();
	            String tListId = taskListId;
	            String title = task.getTitle();
	        	String updated = task.getUpdated();
	        	String notes = task.getNotes();
	        	String status = task.getStatus();
	        	String isDirty = String.valueOf(task.isDirty());
	        	String isDeleted = String.valueOf(task.isDeleted());
	        	String created = String.valueOf(task.getCreated());
	            
	            if(id!=null){statement.bindString(columnIndex++,id);}else{columnIndex++;}
	            if(tListId!=null){statement.bindString(columnIndex++,tListId);}else{columnIndex++;}
	            if(title!=null){statement.bindString(columnIndex++,title);}else{columnIndex++;}
	            if(updated!=null){statement.bindString(columnIndex++,updated);}else{columnIndex++;}
	            if(notes!=null){statement.bindString(columnIndex++,notes);}else{columnIndex++;}
	            if(status!=null){statement.bindString(columnIndex++,status);}else{columnIndex++;}
	            if(isDirty!=null){statement.bindString(columnIndex++,isDirty);}else{columnIndex++;}
	            if(isDeleted!=null){statement.bindString(columnIndex++,isDeleted);}else{columnIndex++;}
	            if(created!=null){statement.bindString(columnIndex++,created);}else{columnIndex++;}
	            // rest of bindings
	            statement.execute(); //or executeInsert() if id is needed
	        }
	        database.setTransactionSuccessful();
	    } finally {
	        database.endTransaction();
	        close();
	    }
		
	}
	
	public synchronized void addTaskInTable(TaskItem task){
		if(task==null){
			return;
		}
		open();
		String tableName = SqliteOpenHelper.Table_Tasks;
		
		final String INSERT_QUERY = createInsert(tableName, tasksColumns);
		final SQLiteStatement statement = database.compileStatement(INSERT_QUERY);
		database.beginTransaction();
		int columnIndex = 1;
		try {
        	
        	statement.clearBindings();
            columnIndex = 1;
            
            String id = task.getId();
            String tListId = task.getTaskListId();
            String title = task.getTitle();
        	String updated = task.getUpdated();
        	String notes = task.getNotes();
        	String status = task.getStatus();
        	String isDirty = String.valueOf(task.isDirty());
        	String isDeleted = String.valueOf(task.isDeleted());
        	String created = String.valueOf(task.getCreated());
            
            if(id!=null){statement.bindString(columnIndex++,id);}else{columnIndex++;}
            if(tListId!=null){statement.bindString(columnIndex++,tListId);}else{columnIndex++;}
            if(title!=null){statement.bindString(columnIndex++,title);}else{columnIndex++;}
            if(updated!=null){statement.bindString(columnIndex++,updated);}else{columnIndex++;}
            if(notes!=null){statement.bindString(columnIndex++,notes);}else{columnIndex++;}
            if(status!=null){statement.bindString(columnIndex++,status);}else{columnIndex++;}
            if(isDirty!=null){statement.bindString(columnIndex++,isDirty);}else{columnIndex++;}
            if(isDeleted!=null){statement.bindString(columnIndex++,isDeleted);}else{columnIndex++;}
            if(created!=null){statement.bindString(columnIndex++,created);}else{columnIndex++;}
            // rest of bindings
            statement.execute(); //or executeInsert() if id is needed
        	
	        database.setTransactionSuccessful();
	    } finally {
	        database.endTransaction();
	        close();
	    }
		
	}
	
	public synchronized ArrayList<TaskItem> getTasksN(String taskListId){
		ArrayList<TaskItem> tasksArray = new ArrayList<TaskItem>();
		open();
		TaskItem task = null;
		String tableName = SqliteOpenHelper.Table_Tasks;
		String selectQuery = "SELECT  * FROM " + tableName + " WHERE " + SqliteOpenHelper.taskListId + "='" + taskListId + "' AND " + SqliteOpenHelper.isDeleted + "='"+ String.valueOf(false) + "'";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst();
			do{
				task = new TaskItem();
				task.setId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskId)));
				task.setTitle(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.title)));
				task.setTaskListId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskListId)));
				task.setUpdated(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.updated)));
				task.setNotes(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.notes)));
				task.setStatus(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.status)));
				task.setDirty(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDirty))));
				task.setDeleted(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDeleted))));
				task.setCreated(Long.parseLong(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.created))));
				tasksArray.add(task);
			}while(cursor.moveToNext());
		}
		cursor.close();
		close();
		
		return tasksArray;
	}
	
	
	public synchronized ArrayList<TaskItem> getAllDirtyTasksN(){
		ArrayList<TaskItem> tasksArray = new ArrayList<TaskItem>();
		open();
		TaskItem task = null;
		String tableName = SqliteOpenHelper.Table_Tasks;
		String selectQuery = "SELECT  * FROM " + tableName + " WHERE " + SqliteOpenHelper.isDirty + "='" + String.valueOf(true) + "'";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst();
			do{
				task = new TaskItem();
				task.setId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskId)));
				task.setTitle(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.title)));
				task.setTaskListId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskListId)));
				task.setUpdated(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.updated)));
				task.setNotes(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.notes)));
				task.setStatus(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.status)));
				task.setDirty(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDirty))));
				task.setDeleted(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDeleted))));
				task.setCreated(Long.parseLong(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.created))));
				tasksArray.add(task);
			}while(cursor.moveToNext());
		}
		cursor.close();
		close();
		
		return tasksArray;
	}
	
	public synchronized TaskItem getDirtyTaskN(){
		open();
		TaskItem task = null;
		String tableName = SqliteOpenHelper.Table_Tasks;
		String selectQuery = "SELECT  * FROM " + tableName + " WHERE " + SqliteOpenHelper.isDirty + "='" + String.valueOf(true) + "' LIMIT 1";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst();
			task = new TaskItem();
			task.setId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskId)));
			task.setTitle(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.title)));
			task.setTaskListId(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.taskListId)));
			task.setUpdated(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.updated)));
			task.setNotes(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.notes)));
			task.setStatus(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.status)));
			task.setDirty(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDirty))));
			task.setDeleted(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.isDeleted))));
			task.setCreated(Long.parseLong(cursor.getString(cursor.getColumnIndex(SqliteOpenHelper.created))));
		}
		cursor.close();
		close();
		
		return task;
	}
	
	public synchronized void updateTask(TaskItem task){
		open();
		String tableName = SqliteOpenHelper.Table_Tasks;
		
		final String UPDATE_QUERY = createUpdate(tableName, tasksColumns, SqliteOpenHelper.taskId);
		final SQLiteStatement statement = database.compileStatement(UPDATE_QUERY);
		database.beginTransaction();
		int columnIndex = 1;
		try {
        	if(task.getId()!=null && !task.getId().equals("")){
	        	statement.clearBindings();
	            columnIndex = 1;
	            
	            String id = task.getId();
	            String tListId = task.getTaskListId();
	            String title = task.getTitle();
	        	String updated = task.getUpdated();
	        	String notes = task.getNotes();
	        	String status = task.getStatus();
	        	String isDirty = String.valueOf(task.isDirty());
	        	String isDeleted = String.valueOf(task.isDeleted());
	        	String created = String.valueOf(task.getCreated());
	            
	            if(id!=null){statement.bindString(columnIndex++,id);}else{columnIndex++;}
	            if(tListId!=null){statement.bindString(columnIndex++,tListId);}else{columnIndex++;}
	            if(title!=null){statement.bindString(columnIndex++,title);}else{columnIndex++;}
	            if(updated!=null){statement.bindString(columnIndex++,updated);}else{columnIndex++;}
	            if(notes!=null){statement.bindString(columnIndex++,notes);}else{columnIndex++;}
	            if(status!=null){statement.bindString(columnIndex++,status);}else{columnIndex++;}
	            if(isDirty!=null){statement.bindString(columnIndex++,isDirty);}else{columnIndex++;}
	            if(isDeleted!=null){statement.bindString(columnIndex++,isDeleted);}else{columnIndex++;}
	            if(created!=null){statement.bindString(columnIndex++,created);}else{columnIndex++;}
	            statement.bindString(columnIndex++,task.getId());
	            // rest of bindings
	            statement.execute(); //or executeInsert() if id is needed
        	}
	        database.setTransactionSuccessful();
	    } finally {
	        database.endTransaction();
	        close();
	    }
	}
	
	public synchronized void updateTaskWithCreatedDate(TaskItem task){
		open();
		String tableName = SqliteOpenHelper.Table_Tasks;
		
		final String UPDATE_QUERY = createUpdate(tableName, tasksColumns, SqliteOpenHelper.created);
		final SQLiteStatement statement = database.compileStatement(UPDATE_QUERY);
		database.beginTransaction();
		int columnIndex = 1;
		try {
        	statement.clearBindings();
            columnIndex = 1;
            
            String id = task.getId();
            String tListId = task.getTaskListId();
            String title = task.getTitle();
        	String updated = task.getUpdated();
        	String notes = task.getNotes();
        	String status = task.getStatus();
        	String isDirty = String.valueOf(task.isDirty());
        	String isDeleted = String.valueOf(task.isDeleted());
        	String created = String.valueOf(task.getCreated());
            
            if(id!=null){statement.bindString(columnIndex++,id);}else{columnIndex++;}
            if(tListId!=null){statement.bindString(columnIndex++,tListId);}else{columnIndex++;}
            if(title!=null){statement.bindString(columnIndex++,title);}else{columnIndex++;}
            if(updated!=null){statement.bindString(columnIndex++,updated);}else{columnIndex++;}
            if(notes!=null){statement.bindString(columnIndex++,notes);}else{columnIndex++;}
            if(status!=null){statement.bindString(columnIndex++,status);}else{columnIndex++;}
            if(isDirty!=null){statement.bindString(columnIndex++,isDirty);}else{columnIndex++;}
            if(isDeleted!=null){statement.bindString(columnIndex++,isDeleted);}else{columnIndex++;}
            if(created!=null){statement.bindString(columnIndex++,created);}else{columnIndex++;}
            statement.bindString(columnIndex++,created);
            // rest of bindings
            statement.execute(); //or executeInsert() if id is needed
        database.setTransactionSuccessful();
	    } finally {
	        database.endTransaction();
	        close();
	    }
	}
	
	public synchronized void deleteTasksFromTable(String taskListId){
		String tableName = SqliteOpenHelper.Table_Tasks;
		open();
		database.execSQL("DELETE FROM "+tableName + " WHERE " + SqliteOpenHelper.taskListId + "='" + taskListId + "'");
		close();
	}
	
	public synchronized void deleteTaskFromTableWithId(String taskId){
		String tableName = SqliteOpenHelper.Table_Tasks;
		open();
		database.execSQL("DELETE FROM "+tableName + " WHERE " + SqliteOpenHelper.taskId + "='" + taskId + "'");
		close();
	}
	
	public synchronized void deleteTaskFromTableWithCreatedDate(long created){
		String tableName = SqliteOpenHelper.Table_Tasks;
		open();
		database.execSQL("DELETE FROM "+tableName + " WHERE " + SqliteOpenHelper.created + "='" + created + "'");
		close();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                                     HELPER FUNCTIONS                                                //
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void resetCacheTables(boolean isCleanUp){
		open();
		
		if(isCleanUp){
			database.execSQL("DELETE FROM "+SqliteOpenHelper.Table_Task_Lists);
		}
		close();
	}
	
	private String createInsert(final String tableName,
			final String[] columnNames) {
		if (tableName == null || columnNames == null || columnNames.length == 0) {
			throw new IllegalArgumentException();
		}
		final StringBuilder s = new StringBuilder();
		s.append("INSERT INTO ").append(tableName).append(" (");
		for (String column : columnNames) {
			s.append(column).append(" ,");
		}
		int length = s.length();
		s.delete(length - 2, length);
		s.append(") VALUES( ");
		for (int i = 0; i < columnNames.length; i++) {
			s.append(" ? ,");
		}
		length = s.length();
		s.delete(length - 2, length);
		s.append(")");
		return s.toString();
	}

	private String createUpdate(final String tableName,
			final String[] columnNames, String whereSelectionColumnName) {
		if (tableName == null || columnNames == null || columnNames.length == 0
				|| whereSelectionColumnName == null) {
			throw new IllegalArgumentException();
		}
		final StringBuilder s = new StringBuilder();
		s.append("UPDATE ").append(tableName).append(" SET ");
		for (String column : columnNames) {
			s.append(column).append(" = ? ,");
		}
		int length = s.length();
		s.delete(length - 2, length);
		s.append(" WHERE ").append(whereSelectionColumnName).append(" = ?");
		return s.toString();
	}

}