package com.googletasks.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class SqliteOpenHelper extends SQLiteOpenHelper {

	private static SqliteOpenHelper instance = null;
	
	private static final String id = "_id";
	
	//TaskLists
	public static final String taskListId = "taskListId";
    public static final String title = "title";
    
    //Tasks
    public static final String taskId= "taskId";
//    public static final String taskListId= "taskListId";
//    public static final String title= "";
    public static final String updated= "updated";
    public static final String notes= "notes";
    public static final String status= "status";
    public static final String isDirty= "isDirty";
    public static final String isDeleted= "isDeleted";
    public static final String created = "created";
	
	//Table Names
	public static final String Table_Task_Lists = "tbl_task_lists";
	public static final String Table_Tasks = "tbl_tasks";

	public static String DATABASE_NAME = "googletasks.db";
	private static final int DATABASE_VERSION = 1;

	public static SqliteOpenHelper getInstance(Context context) {
		if (instance == null) {
			instance = new SqliteOpenHelper(context.getApplicationContext());
		}
		return instance;
	}

	private SqliteOpenHelper(Context context) {

		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Setting current instance to null. when getInstance will be called new
	 * Object will be returned
	 */
	public static void clearInstance() {
		instance = null;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(getCreateTableTaskLists());
		db.execSQL(getCreateTableTasks());
	}
	
	private String getCreateTableTaskLists(){
		String query = "CREATE TABLE IF NOT EXISTS " + Table_Task_Lists + "("
				+ id + " integer  PRIMARY KEY AUTOINCREMENT DEFAULT NULL,"
				+ taskListId + " TEXT DEFAULT NULL,"
				+ title + " TEXT DEFAULT NULL"
				+ ")";
		return query;
	}
	
	private String getCreateTableTasks(){
		String query = "CREATE TABLE IF NOT EXISTS " + Table_Tasks + "("
				+ id + " integer  PRIMARY KEY AUTOINCREMENT DEFAULT NULL,"
				+ taskId + " TEXT DEFAULT NULL,"
			    + taskListId + " TEXT DEFAULT NULL,"
			    + title + " TEXT DEFAULT NULL,"
			    + updated + " TEXT DEFAULT NULL,"
			    + notes + " TEXT DEFAULT NULL,"
			    + status + " TEXT DEFAULT NULL,"
			    + isDirty + " TEXT DEFAULT NULL,"
			    + isDeleted + " TEXT DEFAULT NULL,"
			    + created + " TEXT DEFAULT NULL"
				+ ")";
		return query;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SqliteOpenHelper.class.getName(),"Upgrading database from version " + oldVersion + " to "+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + Table_Task_Lists);
		db.execSQL("DROP TABLE IF EXISTS " + Table_Tasks);
		onCreate(db);
	}
}
